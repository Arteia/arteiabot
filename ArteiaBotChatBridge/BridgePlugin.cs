﻿using System;

using ArteiaBot.Bot.Event;
using ArteiaBot.Bot.Message;
using ArteiaBot.Bot.Plugin;
using ArteiaBot.Discord.API.WebSocket;

namespace ArteiaBotChatBridge
{
	public class ChatBridgePlugin : IPlugin
	{
		private string name = "ChatBridge";
		private EventLoop evtLoop;

		public ChatBridgePlugin ()
		{
		}

		public string Name
		{
			get
			{
				return this.name;
			}
		}

		public void initialize(ref EventLoop evtLoop)
		{
			this.evtLoop = evtLoop;

			Console.WriteLine (" - [ChatBridge] Initializing " + this.Name);

			Console.WriteLine (" - [ChatBridge] Found EventLoop Instance");
			this.evtLoop.addEventListener (ArteiaBot.Bot.Event.EventType.TWITCH_MESSAGE_RECV, this.bridge);
		}

		public void bridge(ArteiaBot.Bot.Message.AbstractMessage am)
		{
			ArteiaBot.Discord.API.WebSocket.Channel channel = new ArteiaBot.Discord.API.WebSocket.Channel(160152389607882752);
			string content = am.getMessage ();
			if (am.getEventType () == ArteiaBot.Bot.Event.EventType.TWITCH_MESSAGE_RECV) {
				channel.sendMessage (" - [ChatBridge] [FROM TWITCH] " + content);
			} else if (am.getEventType () == ArteiaBot.Bot.Event.EventType.DISCORD_MESSAGE_CREATE) {
				// TODO
			}
		}
	}
}

