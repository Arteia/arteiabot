﻿using System;

namespace ArteiaBot
{
	public class Program
	{
		/**
		 * Entry point! Sets up a new ArteiaBot instance with given OAuth and Username
		 * 
		 * @param string[] args - Command line arguments
		 */
		static void Main(string[] args)
		{			
			ArteiaBot A = new ArteiaBot ();
			A.run ();
		}
	}
}

