﻿// Disable some async warnings which we know are there
// but aren't really relevant...
#pragma warning disable 4014, 1998

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ArteiaBot.Bot.Event;
using ArteiaBot.Bot.Plugin;

namespace ArteiaBot
{
	/**
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public class ArteiaBot
	{
		private Twitch.Net.Client tClient = null;
		private Discord.Net.Client dClient = null;
		private EventLoop evLoop;

		/**
		 * Sets up a new ArteiaBot instance
		 */
		public ArteiaBot()
		{
			Console.WriteLine ("========================");
			Console.WriteLine (" Initializing ArteiaBot ");
			Console.WriteLine ("========================");

			Bot.UI.UserInterface ui = new Bot.UI.UserInterface ();
			Bot.Config.load ();

			if (Bot.Config.exists ("Discord"))
			{
				Dictionary<string, string> discordCfg = Bot.Config.getConfigFor ("Discord");
				dClient = new Discord.Net.Client (discordCfg["Email"], discordCfg["Password"]);
			}

			if (Bot.Config.exists ("Twitch"))
			{
				Dictionary<string, string> twitchCfg = Bot.Config.getConfigFor ("Twitch");
				this.tClient = new Twitch.Net.Client (twitchCfg["Username"], twitchCfg["OAuth"]);
				this.tClient.setChannel (twitchCfg["Channel"]);
			}

			this.evLoop = new EventLoop ();

			PluginManager.Instance.bindEventLoop(ref evLoop);
			PluginManager.Instance.loadAll();
			PluginManager.Instance.initializeAll ();

			//this.evLoop.addEventListener(EventType.ALL, this.debugCallback);
		}

		public void debugCallback(Bot.Message.AbstractMessage am)
		{
			Console.WriteLine ("[Debug] [" + am.getEventType() + "] MESSAGE QUEUED AND READ");
		}

		/**
		 * Runs the entire program and kicks off the connection to Twitch Chat
		 * 		God save our Kappas
		 * 
		 * While async, running this synchronously is fine. It needs to be async
		 * to allow everything else to be async iirc...
		 * 
		 * @return void
		 */
		public async void run()
		{
			if (this.dClient != null)
			{
				this.dClient.bindEventLoop (ref this.evLoop);
				bool dConnected = this.dClient.connect ();
				if (!dConnected) {
					Console.WriteLine ("[WARNING] Unable to connect to Discord");
				}
				//Console.WriteLine ("Connected to Discord!");
			}

			if (this.tClient != null)
			{
				// Bind the event loop so the Twitch Chat client has a reference to it
				// to queue events whenever an action happens.
				this.tClient.bindEventLoop (ref this.evLoop);

				bool tConnected = false;
				bool loggedIn = false;
				bool joined = false;

				// Practically a wrapper for TcpClient.connect()
				tConnected = this.tClient.connect ();
				if (tConnected) {
					loggedIn = this.tClient.login ();
					joined = this.tClient.joinChannel ();
					// Delegate to async non-blocking task
					Task.Run (() => tClient.listen ());
				} else {
					Console.WriteLine ("[WARNING] Unable to connect to Twitch");
				}
			}

			// Loops forever... jk nothing lasts forever ;-;
			await this.evLoop.run ();
		}
	}
}

