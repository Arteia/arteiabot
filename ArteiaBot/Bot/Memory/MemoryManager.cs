﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Bot.Memory
{
	public class MemoryManager
	{
		private Dictionary<string, Dictionary<string, AbstractMemoryObject>> memoryContainer;

		private static readonly MemoryManager instance = new MemoryManager();

		static MemoryManager() { }

		private MemoryManager() {
			this.memoryContainer = new Dictionary<string, Dictionary<string, AbstractMemoryObject>> ();
		}

		public static MemoryManager Instance
		{
			get
			{
				return instance;
			}
		}

		public bool exists (string ns)
		{
			return this.memoryContainer.ContainsKey (ns);
		}

		public bool exists(string ns, string id)
		{
			if (this.memoryContainer.ContainsKey (ns)) {
				if (this.memoryContainer [ns].ContainsKey (id)) {
					return true;
				}
			}

			return false;
		}

		public void register(string ns, string id, AbstractMemoryObject memory)
		{
			if (!this.exists(ns)) {
				this.memoryContainer.Add (ns, new Dictionary<string, AbstractMemoryObject> ());
			}

			if (!this.exists (ns, id)) {
				this.memoryContainer [ns].Add (id, memory);
			} else {
				this.memoryContainer [ns] [id] = memory;
			}
		}

		public void registerNamespace(string ns)
		{
			if (!this.exists(ns)) {
				this.memoryContainer.Add (ns, new Dictionary<string, AbstractMemoryObject> ());
			}
		}

		/**
		 * 
		 */
		public AbstractMemoryObject read(string ns, string id)
		{
			return this.memoryContainer [ns] [id];
		}

		/**
		 * Returns the memory container of the namespace
		 */
		public Dictionary<string, AbstractMemoryObject> readContainer(string ns)
		{
			return this.memoryContainer [ns];
		}
	}
}

