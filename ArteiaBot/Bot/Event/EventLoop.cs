﻿#pragma warning disable 4014, 1998

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using ArteiaBot.Bot.Event;
using ArteiaBot.Bot.Message;

namespace ArteiaBot.Bot.Event
{
	/**
	 * The event loop object is the core of ArteiaBot. After writing this I have
	 * since found out that C# does have a way to do event loops, but I personally
	 * like the way I've implemented this.
	 * 
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public class EventLoop
	{
		// This is our container of event listener callbacks which we fire off
		private Dictionary<EventType, List<Action<AbstractMessage>>> listeners;
		// This is our queue with event data
		private Queue<KeyValuePair<EventType, AbstractMessage>> msgQueue;
		// Our container of timers
		private Dictionary<string, System.Timers.Timer> timers;
		// We can determine if our loop is running with this
		public bool Running = false;

		/**
		 * Constructor initializes the containers
		 */
		public EventLoop ()
		{
			this.msgQueue = new Queue<KeyValuePair<EventType, AbstractMessage>>();
			this.listeners = new Dictionary<EventType, List<Action<AbstractMessage>>> ();
			this.timers = new Dictionary<string, System.Timers.Timer> ();
		}

        /**
         * Adds a timer to the event loop
         * 
         * @param string timerName - the name of the timer, acts as an identifier
         * @param long timerDuration - The duration of the timer
         * @param Action<string> callback - The callback method to call when the timer finishes
         * @param bool loop - Whether the timer should loop
         */
		public void addTimer(string timerName, long timerDuration, Action<string> callback, bool loop = false)
		{
            // Initialize timer
			System.Timers.Timer t = new System.Timers.Timer();
			t.Interval = timerDuration;
			
            t.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) => {
				callback(e.SignalTime.ToString());
			};

			this.timers.Add (timerName, t);

			if (this.Running) {
				t.Start ();
			}
		}

		/**
		 * Helper method to return whether the loop is running or not
		 * 
		 * @return bool
		 */
		public bool isRunning()
		{
			return this.Running;
		}

		/**
		 * @param ArteiaBot.Event.EventType
		 * @param Action<AbstractMessage> callback - Method to be passed in with an AbstractMessage arg
		 */
		public void addEventListener(EventType eventType, Action<AbstractMessage> callback)
		{
			if (!this.listeners.ContainsKey (eventType))
			{
				this.listeners.Add (eventType, new List<Action<AbstractMessage>> ());
			}
			this.listeners[eventType].Add(callback);
		}

		/**
		 * Queues a message with a given EventType and Message
		 * @param ArteiaBot.Bot.Event.EventType et - The event type so the listener knows what callbacks to call
		 * @param AriaBot.Bot.Message.AbstractMessage am - The message object to pass to the event callback
		 */
		public void queueMessage(EventType et, AbstractMessage am)
		{
			am.setEventType (et);
			this.msgQueue.Enqueue (new KeyValuePair<EventType, AbstractMessage>(et, am));
		}

		/**
		 * Effectively waits for an event to be queued and returns it
		 * 
		 * @return Task<KeyValuePair<EventType, AbstractMessage>> - The EventType and message object in the queue
		 */
		public async Task<KeyValuePair<EventType, AbstractMessage>> peekMessage()
		{
			while(this.msgQueue.Count == 0) {
				// Kill time
			}
			return this.msgQueue.Dequeue ();
		}

		/**
		 * Runs the main event loop. This should be called with:
		 * 		await eventLoop.run();
		 */
		public async Task run()
		{
			this.Running = true;

			if (this.timers.Count > 0) {
				foreach(KeyValuePair<string, System.Timers.Timer> pair in this.timers)
				{
					pair.Value.Start ();
				}
			}

			// Define the key val pair which we can keep overwriting
			KeyValuePair<EventType, AbstractMessage> evt;
			while (true)
			{
				try {
					// Wait for a message
					evt = await this.peekMessage();

					// See if we have event listeners
					if(this.listeners.ContainsKey(evt.Key))
					{
						// Fire all callbacks
						foreach(Action<AbstractMessage> evtCallback in this.listeners[evt.Key])
						{
							//Console.WriteLine("Read Queue! [" + evt.ToString() + "]");
							evtCallback(evt.Value);
						}
					}

					// 
					if(this.listeners.ContainsKey(EventType.ALL))
					{
						// Fire all callbacks
						foreach(Action<AbstractMessage> evtCallback in this.listeners[EventType.ALL])
						{
							//Console.WriteLine("Read Queue! [" + evt.ToString() + "]");
							evtCallback(evt.Value);
						}
					}
				}
				catch(Exception e)
				{
					// TODO : Create a BotMessage and have this emit EventType.BOT_ERROR
					Console.WriteLine ("[EXCEPTION] Whoopsies! Something went wrong!");
					Console.WriteLine (e.Message);
				}
			}
		}
	}
}
