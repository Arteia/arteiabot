﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Bot.UI.Control
{
	public class Router
	{
		private static readonly Router instance = new Router ();

		private Dictionary<string, AbstractController> controllers;

		static Router() { }

		private Router() {
			this.controllers = new Dictionary<string, AbstractController> ();
		}

		public static Router Instance
		{
			get
			{
				return instance;
			}
		}

		public void addController(AbstractController controller)
		{
			string controllerName = controller.Name;

			if (controllerName == "") {
				Type t = controller.GetType ();
			}

			this.controllers.Add (controllerName, controller);
		}

		public AbstractController route(string uri)
		{
			//string controllerRoute = 
			if (uri.StartsWith ("/")) {
				uri = uri.Substring (1);
			}
			string[] parts = uri.Split ('/');

			AbstractController controller = null;

			if (!this.controllers.ContainsKey (parts [0])) {
				return null;
			}

			Console.WriteLine (controller.GetType().ToString());

			return this.controllers [parts [0]];
		}
	}
}
