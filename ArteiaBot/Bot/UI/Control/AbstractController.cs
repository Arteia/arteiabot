﻿using System;

namespace ArteiaBot.Bot.UI.Control
{
	public abstract class AbstractController
	{
		private string name = "";

		public AbstractController () { }

		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}
	}
}

