﻿using System;
using ArteiaBot.Bot.Net;
using ArteiaBot.Bot.Event;

namespace ArteiaBot.Bot.Message
{
	/**
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public abstract class AbstractMessage
	{
		protected string message;
		protected EventType type;
		protected AbstractClient client;

		public AbstractMessage()
		{
		}

		public AbstractMessage(string message, ref AbstractClient client)
		{
			this.message = message;
			this.client = client;
		}

		public void setEventType(EventType type)
		{
			this.type = type;
		}

		public EventType getEventType()
		{
			return this.type;
		}

		public string getMessage()
		{
			return this.message;
		}

		public void setMessage(string message)
		{
			this.message = message;
		}

		public AbstractClient getClient()
		{
			return this.client;
		}

		public void setClient(ref AbstractClient client)
		{
			this.client = client;
		}
	}
}
