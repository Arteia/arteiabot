﻿using System;

namespace ArteiaBot.Bot.Net
{
	public enum WebRequestType
	{
		GET		= 1,
		POST	= 2,
		PATCH	= 3,
		DELETE	= 4
	}
}

