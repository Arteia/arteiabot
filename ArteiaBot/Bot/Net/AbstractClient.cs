﻿#pragma warning disable 0162

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

using ArteiaBot.Bot.Event;
using WebSocketSharp;

namespace ArteiaBot.Bot.Net
{
	/**
	 * 
	 */
	public abstract class AbstractClient
	{
		protected Dictionary<string, KeyValuePair<ConnectionType, AbstractConnection>> connections;

		protected string		username;
		protected string		token;

		protected EventLoop		evtLoop;

		public bool Connected = false;

		/**
		 * 
		 */
		public AbstractClient() {
			this.connections = new Dictionary<string, KeyValuePair<ConnectionType, AbstractConnection>> ();
		}

		/**
		 * 
		 */
		public AbstractClient (ConnectionType type, string host, UInt16 port = 80)
		{
			this.connections = new Dictionary<string, KeyValuePair<ConnectionType, AbstractConnection>> ();

			switch (type) {
			case ConnectionType.UDP:
				AbstractConnection udpConn = new UDPConnection (host, port);
				this.connections.Add("Main", new KeyValuePair<ConnectionType, AbstractConnection>(type, udpConn));
				break;
			case ConnectionType.TCP:
			case ConnectionType.NONE:
			default:
				AbstractConnection tcpConn = new TCPConnection (host, port);
				this.connections.Add("Main", new KeyValuePair<ConnectionType, AbstractConnection>(type, tcpConn));
				break;
			}
		}

		/**
		 * Retrieves the network stream for a specified connection
		 * 
		 * @param string connectionID - The connection ID requested
		 */
		public NetworkStream getNetStream(string connectionID)
		{
			switch (this.connections [connectionID].Key)
			{
			/*case ConnectionType.WEB:
				break;
			case ConnectionType.UDP:
				break;
			case ConnectionType.WEBSOCKET:
				break;*/
			case ConnectionType.TCP:
			default:
				TCPConnection tcpConn = this.connections [connectionID].Value as TCPConnection;
				return tcpConn.getClient ().GetStream ();
				break;
			}
		}

		/**
		 * Sets a user name
		 * 
		 * @param string username - The username to set
		 */
		public void setUsername(string username)
		{
			this.username = username;
		}

		/**
		 * Sets an authentication token
		 * 
		 * @param string token - The token to set
		 */
		public void setToken(string token)
		{
			this.token = token;
		}

		/**
		 * Binds the event loop to the Client object for event emitting
		 * 
		 * @param ref ArteiaBot.Bot.Event.EventLoop evLoop - The event loop to bind (by reference)
		 */
		public void bindEventLoop(ref EventLoop evLoop)
		{
			this.evtLoop = evLoop;
		}

		/**
		 * Adds a connection
		 * 
		 * @param string connectionID - The name of the connection
		 * @param ArteiaBot.Bot.Net.ConnectionType type - The type of client to bind
		 * @param ArteiaBot.Bot.Net.AbstractConnection connection - The connection object to add
		 */
		public void addConnection(string connectionID, ConnectionType type, AbstractConnection connection)
		{
			if (this.connections.ContainsKey(connectionID)) {
				throw new Exception ("Connection with given name already exists : " + connectionID);
			}
			this.connections.Add (connectionID, new KeyValuePair<ConnectionType, AbstractConnection> (type, connection));
		}

		/**
		 * A method to retrieve a connection from the connection pool with a given ID.
		 * This trusts that the developer knows what's being returned as it has a
		 * 'dynamic' return type.
		 * 
		 * @param string connectionID - The name of the connection being requested
		 * @return dynamic - The connection
		 */
		public dynamic getConnection(string connectionID)
		{
			if(!this.connections.ContainsKey(connectionID)) {
				throw new Exception ("[Exception] (Bot) - Connection requested does not exist");
			}
			return this.connections [connectionID].Value;
		}

		/**
		 * Creates a new connection with a given string ID, ConnectionType, host and port.
		 * 
		 * @param string connectionID
		 * @param ArteiaBot.Bot.Net.ConnectionType type
		 * @param string host
		 * @param UInt16 port
		 */
		public void create(string connectionID, ConnectionType type, string host = "127.0.0.1", UInt16 port = 0)
		{
			if (this.connections.ContainsKey(connectionID)) {
				throw new Exception ("Connection with given name already exists : " + connectionID);
			}

			AbstractConnection conn;

			switch (type)
			{
			case ConnectionType.WEB:
				conn = new WebConnection ();
				break;
			case ConnectionType.WEBSOCKET:
				if (port == 0) {
					port = 80;
				}
				conn = new WebSocketConnection (host, port);
				break;
			case ConnectionType.TCP:
			default:
				conn = new TCPConnection (host, port);
				break;
			}

			this.connections.Add(connectionID, new KeyValuePair<ConnectionType, AbstractConnection>(type, conn));
		}

		/**
		 * Sets up the TcpClient and sets the network stream
		 * 
		 * Emits Events:
		 * 		BOT_ERROR
		 * 
		 * @param void
		 */
		public bool connect(string connectionID = "Main")
		{
			try
			{
				if(connectionID == "All")
				{
					foreach(KeyValuePair<string, KeyValuePair<ConnectionType, AbstractConnection>> t in this.connections)
					{
						//this._connect(t.Value.Key, t.Value.Value);
						t.Value.Value.connect();
					}
				}
				else
				{
					//this._connect(this.connections[connectionID].Key, this.connections[connectionID].Value);
					this.connections[connectionID].Value.connect();
				}
			}
			catch(Exception e)
			{
				Bot.Message.BotMessage msg = new Bot.Message.BotMessage ();
				msg.setMessage (e.Message);

				Console.WriteLine ("[ERROR] (Bot) - " + e.Message);

				evtLoop.queueMessage (EventType.BOT_ERROR, msg);

				return false;
			}
			return true;
		}
	}
}

