﻿using System;

namespace ArteiaBot.Bot.Net
{
	public enum ConnectionType
	{
		NONE		= 0,
		CUSTOM		= 1,
		TCP			= 2,
		UDP			= 3,
		WEB			= 4,
		WEBSOCKET	= 5
	}
}

