﻿using System;
using System.Linq;
using WebSocketSharp;

namespace ArteiaBot.Bot.Net
{
	/**
	 * WebSocket wrapper for ArteiaBot connection handling
	 * WebSocketConnection handles all websocket connection based things
	 * 
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public class WebSocketConnection : AbstractConnection
	{
		// Parameters which are specific to WebSocket
		protected string[] protocols;
		protected string fullHostString;

		/**
		 * Empty constructor - still populates protocols because this
		 * object manages it.
		 */
		public WebSocketConnection ()
		{
			this.protocols = new string[0];
		}

		/**
		 * Constructor
		 * 
		 * @param string host - The hostname/ip to connect to
		 */
		public WebSocketConnection(string host)
		{
			this.host = host;
			this.port = 80;
			this.protocols = new string[0];

			this.createConnection ();
		}

		/**
		 * Constructor
		 * 
		 * @param string host - The hostname/ip to connect to
		 * @param UInt16 port - The remote port to connect to
		 */
		public WebSocketConnection(string host, UInt16 port = 80)
		{
			this.host = host;
			this.port = port;
			this.protocols = new string[0];

			this.createConnection ();
		}

		/**
		 * Constructor
		 * 
		 * @param string host - The hostname/ip to connect to
		 * @param string[] protocols - Protocols to use with the websocket
		 */
		public WebSocketConnection(string host, string[] protocols = null)
		{
			this.host = host;
			this.port = 80;
			this.protocols = protocols ?? new string[0];

			this.createConnection ();
		}

		/**
		 * Constructor
		 * 
		 * @param string host - The hostname/ip to connect to
		 * @param UInt16 port - The remote port to connect to
		 * @param string[] protocols - Protocols to use with the websocket
		 */
		public WebSocketConnection(string host, UInt16 port = 80, string[] protocols = null)
		{
			this.host = host;
			this.port = port;
			this.protocols = protocols ?? new string[0];

			this.createConnection ();
		}

		/**
		 * Sets the protocols to the given array of protocols.
		 * 
		 * @param string[] protocols - the given array of protocols
		 */
		public void setProtocols(string[] protocols)
		{
			this.protocols = protocols;
		}

		/**
		 * I have no idea if this actually works but this is effectively
		 * a way to add a protocol to the protocols array
		 * 
		 * @param string protocol - the protocol to add
		 */
		public void addProtocols(string protocol)
		{
			int nLength = this.protocols.Length + 1;
			string[] nProtocols = new string[nLength];
			this.protocols.CopyTo (nProtocols, 0);
			this.protocols = nProtocols.Concat (new[] {protocol}).ToArray();
		}

		/**
		 * Creates the connection with the set host/port. Will default to pre-given
		 * host and port if null.
		 * 
		 * @param string host (null) - The hostname/ip to connect to
		 * @param UInt16 port (null) - The remote port to connect to
		 */
		protected void createConnection(string host = null, UInt16 port = 0)
		{
			host = host ?? this.host;
			port = port == 0 ? port : this.port;
			string[] protocols = this.protocols;
			string fullHostString = this.host;

			if(!host.Contains("ws") && host.IndexOf("ws") != 0) {
				if (host.Contains ("://")) {
					throw new Exception ("No scheme defined for WebSocketConnection");
				}

				this.fullHostString = "ws://" + host;
			}

			if(port != 80) {
				this.fullHostString = this.fullHostString + ":" + port.ToString ();
			}

			WebSocket wsClient = new WebSocket (fullHostString, protocols);
			this.setClient<WebSocket> (wsClient);
		}

		/**
		 * 
		 */
		public override bool connect() {
			WebSocket wsClient = this.getClient<WebSocket> ();

			wsClient.Connect ();
			return true;
		}

		public void send(string data)
		{
			WebSocket wsClient = this.getClient<WebSocket> ();
			wsClient.Send (data);
		}

		public void onReceive(Action<string> callback)
		{
			WebSocket wsClient = this.getClient<WebSocket> ();

			wsClient.OnMessage += (sender, e) => {
				//Console.WriteLine ("Response: " + e.Data);
				callback(e.Data);
			};
		}
	}
}

