﻿using System;
using System.Net.Sockets;

namespace ArteiaBot.Bot.Net
{
	/**
	 * AbstractConnection for goody polymorphism black magicks
	 * 
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public abstract class AbstractConnection
	{
		protected dynamic	client;
		protected string	host;
		protected UInt16	port;

		protected NetworkStream		netStream;
		protected Byte[]			buffer;

		public AbstractConnection() { }

		/**
		 * 
		 */
		public AbstractConnection (string host, UInt16 port)
		{
			this.host = host;
			this.port = port;
		}

		public void setHost(string host)
		{
			this.host = host;
		}

		public void setPort(UInt16 port)
		{
			this.port = port;
		}

		/**
		 * Sets the client
		 * 
		 * @param T client - The client to set against this connection
		 */
		protected void setClient<T>(T client)
		{
			this.client = client;
		}

		/**
		 * @return T - The client to get from this connection
		 */
		protected T getClient<T>()
		{
			return (T)(object)this.client;
		}

		public abstract bool connect();
	}
}

