﻿using System;
using System.Net;

using Newtonsoft.Json;

namespace ArteiaBot.Bot.Net
{
	public class WebConnection : AbstractConnection
	{
		public WebConnection ()
		{
			WebClient webClient = new WebClient ();
			this.setClient<WebClient> (webClient);
		}

		public WebConnection(string host, UInt16 port)
		{
			this.host = host;
			this.port = port;

			WebClient webClient = new WebClient ();
			this.setClient<WebClient> (webClient);
		}

		public string request(string uri, WebRequestType requestType = WebRequestType.GET, string body = null)
		{
			WebClient webClient = this.getClient<WebClient> ();

			if (requestType == WebRequestType.POST) {
				webClient.Headers [HttpRequestHeader.ContentType] = "application/json";
			}

			string response = webClient.UploadString (uri, body);

			return response;
		}

		public override bool connect()
		{
			return false;
		}
	}
}

