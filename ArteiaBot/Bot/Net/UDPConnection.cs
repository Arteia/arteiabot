﻿using System;

namespace ArteiaBot.Bot.Net
{
	/**
	 * UdpClient wrapper for ArteiaBot connection handling
	 * 
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public class UDPConnection : AbstractConnection
	{
		public UDPConnection (string host, UInt16 port)
		{
			this.host = host;
			this.port = port;
		}

		public override bool connect()
		{
			return false;
		}
	}
}

