﻿using System;
using System.Net.Sockets;

namespace ArteiaBot.Bot.Net
{
	/**
	 * TcpClient wrapper for ArteiaBot connection handling
	 * 
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public class TCPConnection : AbstractConnection
	{
		public TCPConnection (string host, UInt16 port)
		{
			this.setClient<TcpClient> (new TcpClient ());
			this.host = host;
			this.port = port;
		}

		public override bool connect()
		{
			this.client.Connect (this.host, this.port);
			return true;
		}

		public TcpClient getClient()
		{
			return this.getClient<TcpClient> ();
		}
	}
}

