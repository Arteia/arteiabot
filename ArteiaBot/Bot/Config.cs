﻿using System;
using System.IO;

using System.Collections.Generic;

using Newtonsoft.Json;

namespace ArteiaBot.Bot
{
	public static class Config
	{
		private static ConfigFile cFile = null;

		public static void load()
		{
			if (Config.cFile == null) {
				Config.cFile = new ConfigFile ();
			}

			//File.Load(Application.StartupPath + @"\config\file.xml");
			string cDir = Directory.GetCurrentDirectory();
			string file = Path.DirectorySeparatorChar +  "config" + Path.DirectorySeparatorChar + "bot.json";

			if (!File.Exists (cDir + file)) {
				Config.cFile = null;
				return;
			}

			string fileContents = File.ReadAllText (cDir + file);

			Dictionary<string, Dictionary<string, string>> cfg = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>> (fileContents);
			Config.cFile.setConfig (cfg);
		}

		public static bool exists(string botType)
		{
			if (Config.cFile == null) {
				Config.load ();
			}

			if (Config.cFile == null) {
				return false;
			}

			return Config.cFile.exists (botType);
		}

		public static Dictionary<string, string> getConfigFor(string botType)
		{
			if (Config.cFile == null) {
				Config.load ();
			}

			if (Config.cFile == null) {
				throw new Exception ("[ERROR] (Bot) - Config file does not exist");
			}

			return Config.cFile.get (botType);
		}
	}
}

