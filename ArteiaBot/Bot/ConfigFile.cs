﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Bot
{
	public class ConfigFile
	{
		private Dictionary<string, Dictionary<string, string>> config;

		public ConfigFile () {
			config = new Dictionary<string, Dictionary<string, string>> ();
		}

		public void setConfig(Dictionary<string, Dictionary<string, string>> cfg)
		{
			this.config = cfg;
		}

		public bool exists(string key)
		{
			return this.config.ContainsKey (key);
		}

		public Dictionary<string, string> get(string key)
		{
			return this.config [key];
		}
	}
}

