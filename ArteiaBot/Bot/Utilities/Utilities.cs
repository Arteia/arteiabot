﻿using System;

namespace ArteiaBot.Bot.Utilities
{
	public class Nonce
	{
		private static Random generator = new Random();

		public Nonce() { }

		public static long generate()
		{
			return Convert.ToInt64(generator.Next(1, int.MaxValue));
		}
	}
}

