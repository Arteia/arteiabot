﻿using System;

using ArteiaBot.Bot.Event;

namespace ArteiaBot.Bot.Plugin
{
    public interface IPlugin
    {
        string Name
        {
            get;
        }

		void initialize(ref EventLoop evtLoop);
    }
}
