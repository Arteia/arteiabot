﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using ArteiaBot.Bot.Event;

namespace ArteiaBot.Bot.Plugin
{
	public class PluginManager
	{
		private static readonly PluginManager instance = new PluginManager();

		private Type typePlugin = typeof(IPlugin); 
        
		private Dictionary<string, string> dllFileNames;
        private Dictionary<string, KeyValuePair<Type, IPlugin>> plugins;

		private EventLoop evtLoop;

        /**
         * 
         */
		static PluginManager() { }

        /**
         * 
         */
		private PluginManager()
		{
			Console.WriteLine (" - Initializing PluginManager");

			this.dllFileNames = new Dictionary<string, string>();
			this.plugins = new Dictionary<string, KeyValuePair<Type, IPlugin>>();

			string path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "plugins";

			string[] dllFileNames = null;
			if (Directory.Exists(path))
			{
				string[] tmp = null;
				string filename = "";
				int tmpLen = 0;

				dllFileNames = Directory.GetFiles(path, "*.dll");
				foreach(string file in dllFileNames)
				{
					tmp = file.Split(Path.DirectorySeparatorChar);
					tmpLen = tmp.Length;
					filename = tmp[tmpLen - 1];

					tmp = filename.Split('.');
					tmpLen = tmp.Length;
					filename = "";

					for (int i = 0; i < tmpLen - 1; i++)
					{
						filename = filename + tmp[i];
					}

					Console.WriteLine("   - Found : " + filename + "");

					this.dllFileNames.Add(filename, file);
				}
			}
		}

        /**
         * 
         */
		public static PluginManager Instance
		{
			get { return instance; }
		}

		public void bindEventLoop(ref EventLoop evtLoop)
		{
			this.evtLoop = evtLoop;
		}

		public void loadAll ()
		{
			if (this.dllFileNames.Count > 0) {
				foreach (KeyValuePair<string, string> dll in this.dllFileNames) {
					Console.WriteLine ("   - Loading plugin : " + dll.Key);

					AssemblyName asmName = AssemblyName.GetAssemblyName (dll.Value);
					Assembly asm = Assembly.Load (asmName);
					//PluginManager.assemblies.Add (dll.Key, asm);

					Type[] types = asm.GetTypes();

					foreach (Type t in types)
					{
						if (t.IsInterface || t.IsAbstract)
						{
							continue;
						}

						if (t.GetInterface (this.typePlugin.FullName) != null)
						{
							IPlugin plugin = (IPlugin)Activator.CreateInstance(t);
							KeyValuePair<Type, IPlugin> kvp = new KeyValuePair<Type, IPlugin> (t, plugin);
							this.plugins.Add (dll.Key, kvp);
						}
					}
				}
			}
		}

		public void initializeAll()
		{
			if (this.plugins.Count > 0) {
				foreach (KeyValuePair<string, KeyValuePair<Type, IPlugin>> plugin in this.plugins) {
					plugin.Value.Value.initialize (ref this.evtLoop);
				}
			}
		}
	}
}

