﻿using System;

using ArteiaBot.Bot.Memory;
using ArteiaBot.Twitch.API;

namespace ArteiaBot.Twitch.Memory
{
	public class ChannelMemoryObject : AbstractMemoryObject
	{
		private Channel channel;

		public ChannelMemoryObject ()
		{
		}

		public void store(Channel obj)
		{
			this.channel = obj;
		}

		public Channel retrieve()
		{
			return this.channel;
		}
	}
}

