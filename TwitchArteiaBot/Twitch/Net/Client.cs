﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using ArteiaBot.Bot.Event;
using ArteiaBot.Bot.Net;
using ArteiaBot.Twitch.API;

namespace ArteiaBot.Twitch.Net
{
	/**
	 * Twitch Chat Client object
	 * 
	 * Manages things like authentication to the server as well as the
	 * network data stream.
	 * 
 	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
 	 */
	public class Client
		: AbstractClient
	{
		// Channel variable
		private Channel channel;
        private User me;

		/**
		 * Constructor with username and oauth token parameters. Calls AbstractClient's
		 * constructor and sets the host:port neatly.
		 * 
		 * @param string username - The user name to log in as
		 * @param string oauth - The oauth token
		 */
		public Client (string username, string oauth)
			: base (ConnectionType.TCP, "irc.twitch.tv", 6667)
		{
			this.username = username;
			this.token = oauth;

			WebConnection webConn = new WebConnection ();
			this.addConnection("Web", ConnectionType.WEB, webConn);
		}

		/**
		 * Set the channel to connect to
		 * 
		 * @param string channel - The channel name to connect to
		 */
        public void setChannel(string channelName)
        {
            this.channel = new Channel(channelName);
        }

        /**
         * Set the channel to connect to
         * 
         * @param ArteiaBot.Twitch.API.Channel channel - The Channel to connect to
         */
        public void setChannel(Channel channel)
        {
            this.channel = channel;
        }

		/**
		 * Sets the OAuth token
		 * 
		 * @param string oauth - OAuth token
		 */
		public void setOAuth(string oauth)
		{
			this.token = oauth;
		}

		/**
		 * Sets the Username
		 * 
		 * @param string username - User name
		 */
		new public void setUsername(string user)
		{
			this.username = user.ToLower();
			this.me = new User (user.ToLower ());
		}

		/**
		 * Binds the event loop to the Discord Client as well as the Discord Message Handler
		 * 
		 * @param ref ArteiaBot.Bot.Event.EventLoop evLoop - The event loop to bind (by reference)
		 */
		new public void bindEventLoop(ref EventLoop evLoop)
		{
			this.evtLoop = evLoop;

			Twitch.Message.MessageHandler.bindEventLoop (ref this.evtLoop);
		}

		/**
		 * Performs the login for Twitch Chat.
		 * Based off Chhopsky's script : https://github.com/chhopsky/twitchat
		 * Thank you based Chhopsky
		 * 
		 * Emits Events:
		 * 		TWITCH_CONNECT
		 * 		TWITCH_ERROR
		 */
		public bool login()
		{
			// Sets the login string and converts to an AoB
			string loginString = "PASS oauth:" + this.token + "\r\nNICK " + this.username + "\r\n";
			Byte[] loginPacket = System.Text.Encoding.ASCII.GetBytes(loginString);

			NetworkStream netStream = this.getNetStream ("Main");
			Byte[] buffer;

			// Write the packet to the stream
			
			netStream.Write(loginPacket, 0, loginPacket.Length);
			Console.WriteLine(" - [TWITCH] Attempting to log in " + this.username);

			// Initialize the buffer
			buffer = new Byte[512];

			// Local response
			String responseData = String.Empty;

			// Read the first batch of the response
			Int32 bytes = netStream.Read(buffer, 0, buffer.Length);
			responseData = System.Text.Encoding.ASCII.GetString(buffer, 0, bytes);

			// Split out the lines into an array
			string[] lines = responseData.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

			// New message to pass to the queue
			Twitch.Message.TwitchMessage msg = new Twitch.Message.TwitchMessage ();

			// If we have a welcome message we GUCCI
			if (lines [0].Contains(":Welcome, GLHF!")) {
				this.evtLoop.queueMessage (EventType.TWITCH_CONNECT, msg);
				Console.WriteLine (" - [TWITCH] Received WELCOME: {0}", lines[0]);

                // Set IRCv3 stuffs
                string v3 = "CAP REQ :twitch.tv/membership\r\n";
                Byte[] v3Packet = System.Text.Encoding.ASCII.GetBytes(v3);
                netStream.Write(v3Packet, 0, v3Packet.Length);

                bytes = netStream.Read(buffer, 0, buffer.Length);
                responseData = System.Text.Encoding.ASCII.GetString(buffer, 0, bytes);

                //Console.WriteLine(responseData.Replace("\r", "").Replace("\n", ""));

				this.evtLoop.addEventListener (EventType.TWITCH_PING, this.pong);

				return true;
			}

			// Error lol
			msg.setMessage (responseData);
			this.evtLoop.queueMessage (EventType.TWITCH_ERROR, msg);

			return false;
		}

		/**
		 * Joins the specified channel
		 * 
		 * return bool - On successful join, returns true.
		 */
        public bool joinChannel()
        {
			NetworkStream netStream = this.getNetStream ("Main");
			Byte[] buffer;

            string ch = this.channel.JoinString();

            string joinstring = "JOIN " + ch + "\r\n";
            Byte[] join = System.Text.Encoding.ASCII.GetBytes(joinstring);
            netStream.Write(join, 0, join.Length);
			Console.WriteLine(" - [TWITCH] Attempting to join " + this.channel);
            //Console.WriteLine(joinstring.Replace("\r\n", ""));

            return true;
        }

        /**
         * @param string msg - The message to send
         */
        public void sendMessage(string msg)
        {
			NetworkStream netStream = this.getNetStream ("Main");
			Byte[] buffer;

            string ch = this.channel.AsString();
            ch = "#" + ch;

            string toSend = this.getChannelString()
                + msg
                + "\r\n";

            Byte[] packet = System.Text.Encoding.ASCII.GetBytes(toSend);
            netStream.Write(packet, 0, packet.Length);
        }

        /**
         * 
         */
        public string getChannelString()
        {
            string ch = this.channel.AsString();
            ch = "#" + ch;

            string channelString = ":" + this.channel
                + "!" + this.channel
                + "@" + this.channel
                + ".tmi.twitch.tv PRIVMSG " + ch + " :";

            return channelString;
        }

        /**
         * Overload to give a channel by string.
         * Will effectively change channels at this stage due to no multi channel support
         * 
         * @param string channel - The new channel to connect to
         */
        public bool joinChannel(string channel)
        {
            this.setChannel(channel);
            
            return this.joinChannel();
        }

        /**
         * A basic echo method to be used as a callback when a message is received
         */
        public void echoChat(Bot.Message.AbstractMessage am)
        {
            string msg = am.getMessage();
            this.sendMessage(msg);
        }

		/**
		 * Pong method to respond to ping.
		 * 
		 * @param ArteiaBot.Bot.Message.AbstractMessage am - The abstract message container param
		 */
        public void pong(Bot.Message.AbstractMessage am)
        {
			NetworkStream netStream = this.getNetStream ("Main");
			Byte[] buffer;

            Byte[] say = System.Text.Encoding.ASCII.GetBytes("PONG :tmi.twitch.tv\r\n");
            netStream.Write(say, 0, say.Length);
        }

		/**
		 * Loops over the socket. Should be running on a non-blocking async thread.
		 * i.e. Task.Run(() => twitchClient.listen(ref evtLoop));
		 * 
		 * @param ref ArteiaBot.Event.EventLoop - A reference to the event loop so that
		 * 		we can queue messages. Handle the event emitting manually here.
		 */
		public void listen()
		{
			NetworkStream netStream = this.getNetStream ("Main");
			Byte[] buffer;

            String responseString = String.Empty;

            byte[] buf = new byte[1024];
            StringBuilder sbMessage = new StringBuilder();
            int bytesRead = 0;

            //evtLoop.addEventListener(Bot.Event.EventType.TWITCH_MESSAGE_RECV, this.echoChat);

            while (true)
            {
                // While there's data available to read
                while (netStream.DataAvailable)
                {
                    try
                    {
                        bytesRead = netStream.Read(buf, 0, buf.Length);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("OH SHIT SOMETHING WENT WRONG\r\n", e);
                    }

                    sbMessage.AppendFormat("{0}", Encoding.ASCII.GetString(buf, 0, bytesRead));
                }

                if (sbMessage.Length > 0)
                {
                    // Extract the message
					responseString = sbMessage.ToString();
					Message.MessageHandler.handle (responseString);
                }

                // Clear the string buffer
                sbMessage.Clear();
            }
        }
	}
}

