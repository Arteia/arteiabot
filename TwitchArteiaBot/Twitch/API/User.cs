﻿using System;

namespace ArteiaBot.Twitch.API
{
    public class User
    {
        private string userName;
        private bool isChannelOwner = false;
        private bool isModerator = false;
        private bool isAdmin = false;

        public User(string userName)
        {
            this.userName = userName;
        }

        public string UserName
        {
            get { return this.userName; }
            set { this.userName = value; }
        }
    }
}
