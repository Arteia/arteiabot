﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Twitch.API
{
	public class Channel
	{
        private string channelName;

        private Dictionary<string, User> users;

        /**
         * Constructor
         * 
         * @param string channelName - The name of the channel to connect to
         */
		public Channel (string channelName)
		{
            this.channelName = channelName.Replace("#", "");
            this.users = new Dictionary<string, User>();
		}

        /**
         * Adds a user registered against the channel. This is to allow us to see
         * if users are connected to the channel for various things.
         * 
         * @param ArteiaBot.Twitch.API.User - The user to add
         */
        public void addUser(User user)
        {
            this.users.Add(user.UserName, user);
        }

        public string ChannelName
        {
            get { return this.channelName; }
            set { }
        }

        /**
         * Gets the channel name as a string
         * 
         * @return string
         */
        public string AsString()
        {
            return this.channelName;
        }

        /**
         * Returns the string in a format to join via TMI
         * 
         * @return string
         */
        public string JoinString()
        {
            return "#" + this.channelName;
        }
	}
}

