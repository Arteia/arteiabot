﻿using System;
using ArteiaBot.Bot.Message;
using ArteiaBot.Twitch.API;

namespace ArteiaBot.Twitch.Message{
	/**
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public class TwitchMessage : AbstractMessage
	{
		private User user;
		private Channel channel;
	
		public TwitchMessage ()
		{
		}

		/**
		 * 
		 */
		public void setUser(User user)
		{
			this.user = user;
		}

		/**
		 * 
		 */
		public User getUser()
		{
			return this.user;
		}

		/**
		 * 
		 */
		public void setChannel(Channel channel)
		{
			this.channel = channel;
		}

		/**
		 * 
		 */
		public Channel getChannel()
		{
			return this.channel;
		}
	}
}
