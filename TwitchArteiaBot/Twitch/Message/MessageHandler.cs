﻿using System;
using ArteiaBot.Bot.Event;
using ArteiaBot.Bot.Memory;

namespace ArteiaBot.Twitch.Message
{
	public static class MessageHandler
	{
		private static EventLoop evtLoop;

		public static void bindEventLoop(ref EventLoop loop)
		{
			MessageHandler.evtLoop = loop;
		}

		public static void handle(string injest)
		{
			// Set up TwitchMessage object to be passed through
			TwitchMessage msg = new Message.TwitchMessage();

			if (injest == "PING :tmi.twitch.tv\r\n") {
				msg.setMessage ("PING :tmi.twitch.tv\r\n");
				MessageHandler.evtLoop.queueMessage (EventType.TWITCH_PING, msg);
				return;
			}

			string[] message = injest.Split(':');
			string[] preamble = message[1].Split(' ');
			string[] sendingUser = null;
			string user = String.Empty;
			Memory.ChannelMemoryObject memObj;

			// Determine channel name
			string channelName = String.Empty;
			Twitch.API.Channel channel = null;
			if (injest.Contains ("#")) {
				channelName = injest.Split ('#')[1];
				if (channelName.Contains (" ")) {
					channelName = channelName.Split (' ') [0];
				}
				if (MemoryManager.Instance.exists ("TWITCH_Channels", channelName)) {
					memObj = (Memory.ChannelMemoryObject)MemoryManager.Instance.read ("TWITCH_Channels", channelName);
					channel = memObj.retrieve ();
				} else {
					channel = new ArteiaBot.Twitch.API.Channel (channelName);
					memObj = new ArteiaBot.Twitch.Memory.ChannelMemoryObject ();
					memObj.store (channel);
					MemoryManager.Instance.register ("TWITCH_Channels", channelName, memObj);
				}
			}

			// The actual message
			string messageStr = String.Empty;

			switch (preamble [1]) {
			case "PRIVMSG":
				sendingUser = preamble [0].Split ('!');
				user = sendingUser [0];
				messageStr = message [2].Replace ("\r", "").Replace ("\n", "");
				msg.setMessage (messageStr);
				msg.setChannel (channel);
				MessageHandler.evtLoop.queueMessage (EventType.TWITCH_MESSAGE_RECV, msg);
				break;
			case "JOIN":
				sendingUser = preamble[0].Split('!');
				msg.setMessage (sendingUser[0]);
				msg.setChannel (channel);
				MessageHandler.evtLoop.queueMessage(EventType.TWITCH_JOIN, msg);
				break;
			default:
				//Console.Write(" - [TWITCH] [NOT HANDLED]");
				//Console.Write(injest);
				break;
			}
		}
	}
}

