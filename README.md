# ArteiaBot - A bot Framework

ArteiaBot is a heavily work in progress and is subject to vast changes. I really advise using this as a base in its current form and DEFINITELY do not use this in any form of production.

## Credits

Thank you to [@Chhopsky](https://twitter.com/chhopsky) for the basis of the Twitch Chat API ([repository](https://github.com/chhopsky/twitchat))

Thanks to sta.blockhead for their C# WebSocket implementation for earlier versions of .NET ([repository](https://github.com/sta/websocket-sharp))

Thanks to Newtonsoft for their fantastic JSON library ([site](http://www.newtonsoft.com/json))

## Notes

> Twitch chat is NOT IRC - the interface that TMI provides is "irc-like" and does not strictly conform to the RFCs.
>
> \- Chhopsky

## Requirements

ArteiaBot is built in C# however it has a strong emphasis on being cross-platform compatible. The project itself started in Mono and has been developed across both Windows and Ubuntu (specifically 14.04).

The project is targeted at .NET 4.5 and should build in both the latest version of Mono as well as Visual Studio 2013 and up.

Disclaimer : I primarily come from a C++ and PHP background so I'm fairly foreign to C# "best practice" programming and its conventions, so please keep that in mind when you're looking through my code!

## Components

At the moment, ArteiaBot contains all of its dependencies in a single solution, along with Twitch and Discord functionality with an example plugin detailing how it's possible to extend functionality through without having to modify the core code.

## Building

For both Windows and Ubuntu (OSX not tested), clone the repo.

### Windows

Both require Visual Studio installed, some people prefer working from the command line though.

#### Visual Studio

1. Open up the solution in Visual Studio
2. Build!

#### CLI

1. `Windows + R` type `cmd` hit `enter`
2. Type `<drive>:\Program Files (x86)\Microsoft Visual Studio <version>\Common7\Tools\vsvars32.bat`
3. Navigate to  directory with `ArteiaBot.sln`
4. Type `msbuild`

### Ubuntu

After following both of these steps, to run it from the command line you'll need to use `mono` to run it. e.g.: `mono ArteiaBot.exe`

#### MonoDevelop

1. Open up the solution in MonoDevelop
2. Build!

#### CLI

1. Install MSC if you don't have it already `sudo apt-get install mono-mcs`
2. Navigate to  directory with `ArteiaBot.sln`
3. Type `mcs ArteiaBot.sln` and hit enter

## Running ArteiaBot

The application directory of ArteiaBot when you're running it should look something like this (although not exact) :

```
ArteiaBot/
|- config/
|-- bot.json
|- plugins/
|-- Plugin.dll
|-- AnotherPlugin.dll
|- ArteiaBot.exe
|- ArteiaBot.dll
|- DiscordArteiaBot.dll
|- NAudio.dll
|- Newtonsoft.Json.dll
|- NLayer.dll
|- NLayer.NAudioSupport.dll
|- TwitchArteiaBot.dll
|- websocket-sharp.dll
```

### Configuration

ArteiaBot now uses a config system. At the moment there's no way to manage this nicely, however there is a sample config file provided to help set you up in the root of the project.

Take a look at the sample configuration file, as it's pretty self explanatory. Remove the `.sample` when you do use it though!

At the moment, the config file lives in a relative directory to the executable called "config". For example `~/ArteiaBot/bin/Debug/config/bot.json`

### Plugins

Plugins live inside of the application directory in a sub folder called `plugins` and must implement the interface `ArteiaBot.Bot.Plugin.IPlugin` in order to be loaded correctly. Plugins must also have references to `ArteiaBot` as well as any other service it utilizes. For example, if a plugin is being written for Discord, the plugin must reference both `ArteiaBot` and `DiscordArteiaBot`.

### Setting up Twitch

For Twitch, instead of passing through a username and password, Twitch requires you to generate an OAuth token for your application. The chat token is a little different to the standard Twitch API one, and you can generate an OAuth token [here](https://twitchapps.com/tmi/)

### Setting up Discord

Discord does not yet have an official API, so unfortunately we need to send along our password with our email. These are the only configuration values set, as the token is obtained from these credentials.

Good luck, have fun!
