﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using ArteiaBot.Bot.Net;
using ArteiaBot.Discord.API.Web;
using ArteiaBot.Discord.Message;
using ArteiaBot.Bot.Event;

namespace ArteiaBot.Discord.Net
{
	/**
	 * The Discord client is slightly different with the fact we need to utilize
	 * the password immediately to use the web client to perform a login call to
	 * be able to retrieve the token. We don't want to store the password anywhere.
	 * 
	 * @author Tom Brewer-Vinga <arteia@arteianstudios.com>
	 */
	public class Client : AbstractClient
	{
		private string wsHost = "";
		private long id = 0;

		public Client (string user, string password)
		{
			DiscordWebConnection webConn = new DiscordWebConnection ();
			this.addConnection("Web", ConnectionType.WEB, webConn);

			this.token = webConn.login (user, password);
			this.wsHost = webConn.getWebSocketGateway ();

			WebSocketConnection wsConn = new WebSocketConnection (this.wsHost);
			this.addConnection ("Main", ConnectionType.WEBSOCKET, wsConn);

			wsConn.onReceive (MessageHandler.handle);
		}

		/**
		 * Binds the event loop to the Discord Client as well as the Discord Message Handler
		 * 
		 * @param ref ArteiaBot.Bot.Event.EventLoop evLoop - The event loop to bind (by reference)
		 */
		new public void bindEventLoop(ref EventLoop evLoop)
		{
			this.evtLoop = evLoop;

			Discord.Message.MessageHandler.bindEventLoop (ref this.evtLoop);
		}

		/**
		 * Registers the heartbeat after the READY response has been received after connecting
		 * 
		 * @param ArteiaBot.Bot.Message.AbstractMessage am - The received message
		 */
		public void registerHeartbeat(Bot.Message.AbstractMessage am)
		{
			Discord.Message.DiscordMessage dm = am as Discord.Message.DiscordMessage;
			string msg = dm.getMessage ();

			API.WebSocket.ReadyMessage m = JsonConvert.DeserializeObject<API.WebSocket.ReadyMessage> (msg);
			API.WebSocket.Ready r = m.d;

			//Console.WriteLine ("Heartbeat set to pulse every " + r.heartbeat_interval + " milliseconds");

			// Add the timer to send the heartbeat
			evtLoop.addTimer ("DiscordHeartbeat", r.heartbeat_interval, this.pulseHeartbeat, true);
		}

		/**
		 * Sets up the memory object for Discord so that it can be called easier from elsewhere
		 * 
		 * @param Bot.Message.AbstractMessage am - The received message
		 */
		public void setupMemory(Bot.Message.AbstractMessage am)
		{
			Discord.Message.DiscordMessage dm = am as Discord.Message.DiscordMessage;
			string msg = dm.getMessage ();

			API.WebSocket.ReadyMessage m = JsonConvert.DeserializeObject<API.WebSocket.ReadyMessage> (msg);
			API.WebSocket.Ready r = m.d;

			this.id = r.user.id;

			Memory.GuildMemoryObject gmo = null;
			Memory.ChannelMemoryObject cmo = null;
			List<Memory.ChannelMemoryObject> cmol = null;

			//Bot.Memory.MemoryManager.Instance.register(

			foreach (Discord.API.WebSocket.Guild g in r.guilds) {
				gmo = new Memory.GuildMemoryObject (g);
				cmol = new List<ArteiaBot.Discord.Memory.ChannelMemoryObject> ();

				foreach (Discord.API.WebSocket.Channel c in g.channels) {
					cmo = new Memory.ChannelMemoryObject (c);
					cmol.Add (cmo);
				}

				gmo.setChannels (cmol);
				Bot.Memory.MemoryManager.Instance.register ("DISCORD_GUILDS", g.name, gmo);
			}
		}

		/**
		 * Method to pulse the heartbeat in the specified timeframe given from the
		 * READY response after connecting to the server
		 * 
		 * @param string date - The date given in DD/MM/YYYY HH:MM:SS AM/PM format
		 */
		public void pulseHeartbeat(string date)
		{
			//Console.WriteLine (date);
			Discord.API.WebSocket.KeepAlive hb = new Discord.API.WebSocket.KeepAlive ();

			this.send (hb);
		}

		/**
		 * Sends data and converts it serializes it to a json objects
		 * 
		 * @param dynamic data
		 */
		public void send(dynamic data)
		{
			WebSocketConnection ws = this.getConnection ("Main");

			try
			{
				//Console.WriteLine ("Sending object type : " + data.ToString());
				ws.send (JsonConvert.SerializeObject(data));
			}
			catch(Exception e)
			{
				Console.WriteLine ("[ERROR] (DISCORD) - Unable to serialize and send data '" + e.Message + "'");
			}
		}

		/**
		 * Attempts to connect to the obtained Websocket that the Gateway
		 * request through the REST Web API gives us.
		 * 
		 * @return bool - If we connected
		 */
		public bool connect()
		{
			WebSocketConnection ws = this.getConnection ("Main");
			DiscordWebConnection wc = this.getConnection ("Web");

			evtLoop.addEventListener (EventType.DISCORD_READY, this.registerHeartbeat);
			evtLoop.addEventListener (EventType.DISCORD_READY, this.setupMemory);
			evtLoop.addEventListener (EventType.DISCORD_MESSAGE_CREATE, this.echo);

			// If we don't connect then we just return false
			if (!ws.connect ()) {
				Console.WriteLine ("Failed to connect to Discord");
				return false;
			}

			Console.WriteLine (" - [DISCORD] Connected to WebSocket : " + this.wsHost);

			Discord.API.Discord d = new Discord.API.Discord ();
			Discord.API.WebSocket.Connect c = new Discord.API.WebSocket.Connect(this.token);

			Discord.API.WebSocket.Channel.bindConnection (wc);

			d.op = 2;
			d.d = c;
			ws.send (JsonConvert.SerializeObject(d));

			return true;
		}

		public void joinVoiceChannel(string channelName)
		{
			
		}

		/**
		 * Debugging method to echo back responses
		 * 
		 * @param ArteiaBot.Bot.Message.AbstractMessage am - The message
		 */
		public void echo(Bot.Message.AbstractMessage am)
		{
			Discord.Message.DiscordMessage dm = am as DiscordMessage;
			API.WebSocket.Message msg = dm.getMessage<API.WebSocket.Message> ();
			API.WebSocket.MessageCreate mc = JsonConvert.DeserializeObject<API.WebSocket.MessageCreate>(msg.d.ToString());

			if (mc.author.id != this.id) {
				string content = mc.content;
				API.WebSocket.Channel c = new ArteiaBot.Discord.API.WebSocket.Channel (mc.channel_id);
				c.sendMessage (content);
			}
		}
	}
}
