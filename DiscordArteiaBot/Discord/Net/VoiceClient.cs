﻿using System;

using ArteiaBot.Bot.Net;
using ArteiaBot.Discord.API.WebSocket;
using ArteiaBot.Discord.Memory;

namespace ArteiaBot
{
	public class VoiceClient : UDPConnection
	{
		private WebConnection webConnection;
		private WebSocketConnection webSocketConnection;

		/**
		 * 
		 */
		public VoiceClient(ref WebConnection webConnection, ref WebSocketConnection wsConnection) : base("", 0)
		{
			this.webConnection = webConnection;
			this.webSocketConnection = wsConnection;
		}

		/**
		 * 
		 */
		public bool joinChannel(Channel channel)
		{
			if (channel.type != "voice") {
				return false;
			}



			return true;
		}

		/**
		 * 
		 */
		public bool joinChannel(ChannelMemoryObject channelMemoryObject)
		{
			return this.joinChannel (channelMemoryObject.Channel);
		}
	}
}

