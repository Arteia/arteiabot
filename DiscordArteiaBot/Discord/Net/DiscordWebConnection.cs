﻿using System;
using System.Net;

using Newtonsoft.Json;

using ArteiaBot.Bot.Net;
using ArteiaBot.Discord.API.Web;

namespace ArteiaBot.Discord.Net
{
	public class DiscordWebConnection : WebConnection
	{
		private string token;

		public DiscordWebConnection () { }

		/**
		 * 
		 */
		public string login(string user, string password)
		{
			Login login = new Login (user, password);
			string response = this.request(Login.URI, WebRequestType.POST, JsonConvert.SerializeObject(login));

			JsonConvert.SerializeObject (login);
			Discord.API.Web.Token tokenObject = JsonConvert.DeserializeObject<Discord.API.Web.Token>(response);
			this.token = tokenObject.token;

			return this.token;
		}

		/**
		 * 
		 */
		public string getWebSocketGateway()
		{
			//Token token = new Token (this.token);

			string gatewayResponse = this.request (Gateway.URI);
			Gateway gateway = JsonConvert.DeserializeObject<Gateway> (gatewayResponse);

			return gateway.url;
		}

		/**
		 * Request a page with a given URI.
		 * 
		 * @param string uri - The URI to request
		 * @param WebRequestType requestType - The method to use, i.e.: GET, POST
		 * @param string body - The request body i.e.: used in a POST request when sending data
		 */
		new public string request(string uri, WebRequestType requestType = WebRequestType.GET, string body = null)
		{
			WebClient webClient = this.getClient<WebClient> ();
			webClient.Headers [HttpRequestHeader.Authorization] = this.token;

			if (requestType == WebRequestType.POST) {
				webClient.Headers [HttpRequestHeader.ContentType] = "application/json";
				return webClient.UploadString (uri, body);
			}

			return webClient.DownloadString (uri);
		}
	}
}

