﻿using System;

using ArteiaBot.Bot.Memory;

using ArteiaBot.Discord.API.WebSocket;

namespace ArteiaBot.Discord.Memory
{
	public class ChannelMemoryObject : AbstractMemoryObject
	{
		private Channel channel;
		private string type;

		public ChannelMemoryObject (Channel channel)
		{
			this.channel = channel;
			this.type = channel.type;
		}

		public string ChannelName
		{
			get { return this.channel.name; }
			set { }
		}

		public long ChannelID
		{
			get { return this.channel.id; }
			set { }
		}

		public string ChannelType
		{
			get { return this.channel.type; }
			set { }
		}

		public Channel Channel
		{
			get { return this.channel; }
			set { }
		}

		public void sendMessage(string msg)
		{
			this.channel.sendMessage (msg);
		}
	}
}

