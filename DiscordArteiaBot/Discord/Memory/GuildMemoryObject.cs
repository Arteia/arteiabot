﻿using System;
using System.Collections.Generic;

using ArteiaBot.Bot.Memory;

using ArteiaBot.Discord.API.WebSocket;

namespace ArteiaBot.Discord.Memory
{
	public class GuildMemoryObject : AbstractMemoryObject
	{
		Guild guild;
		List<ChannelMemoryObject> channels;

		public GuildMemoryObject (Guild guild)
		{
			this.guild = guild;

			this.channels = new List<ChannelMemoryObject> ();
			foreach (Channel c in guild.channels)
			{
				this.channels.Add(new ChannelMemoryObject (c));
			}
		}

		public ChannelMemoryObject getChannel(string channelName)
		{
			foreach (ChannelMemoryObject c in this.channels) {
				if (channelName == c.ChannelName) {
					return c;
				}
			}
			return null;
		}

		public ChannelMemoryObject getChannel(long channelID)
		{
			foreach (ChannelMemoryObject c in this.channels) {
				if (channelID == c.ChannelID) {
					return c;
				}
			}
			return null;
		}

		public void setChannels(List<ChannelMemoryObject> channels)
		{
			this.channels = channels;
		}
	}
}

