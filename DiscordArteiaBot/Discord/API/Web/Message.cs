﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace ArteiaBot.Discord.API.Web
{
	public class Message
	{
		public string content;
		public List<long> mentions;
		public long nonce;
		public bool tts = false;

		private static string URLSegment = "/messages";
		private static Bot.Net.WebRequestType RequestType = Bot.Net.WebRequestType.POST;

		public Message(string content)
		{
			this.content = content;

		}

		public string getURI()
		{
			return Message.URLSegment;
		}

		public static string URI()
		{
			return Message.URLSegment;
		}

		public Bot.Net.WebRequestType getRequestType()
		{
			return Message.RequestType;
		}

		public string AsString()
		{
			this.nonce = Bot.Utilities.Nonce.generate();
			return JsonConvert.SerializeObject (this);
		}
	}
}

