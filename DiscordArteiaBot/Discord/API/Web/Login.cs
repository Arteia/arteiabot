﻿using System;

namespace ArteiaBot
{
	public struct Login
	{
		public string email;
		public string password;

		public static string URI = "https://discordapp.com/api/auth/login";

		public Login(string email, string password)
		{
			this.email = email;
			this.password = password;
		}
	}
}

