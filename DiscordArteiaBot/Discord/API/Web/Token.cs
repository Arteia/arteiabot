﻿using System;

namespace ArteiaBot.Discord.API.Web
{
	public struct Token
	{
		public string token;

		public Token(string token)
		{
			this.token = token;
		}
	}
}

