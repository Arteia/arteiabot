﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public class PermissionOverwrite
	{
		public int		allow;
		public int		deny;
		public long		id;
		public string	type;

		public PermissionOverwrite ()
		{
		}
	}
}

