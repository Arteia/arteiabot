﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public struct PrivateChannel
	{
		public string id;
		public bool is_private;
		public string last_message_id;
		public User recipient;
	}
}

