﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Discord.API.WebSocket
{
	public class Member
	{
		public bool			deaf;
		public string		joined_at;
		public bool			mute;
		public List<string>	roles;
		public User			user;

		public Member ()
		{
			
		}
	}
}

