﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Discord.API.WebSocket
{
	public struct Guild
	{
		public long id;
		public bool unavailable;
		public string afk_channel_id;
		public long afk_timeout;
		public List<Channel> channels;
		public string icon;
		public string joined_at;
		public bool large;
		public List<Member> members;
		public string name;
		public long owner_id;
		public List<Presence> presences;
		public string region;

	}
}

