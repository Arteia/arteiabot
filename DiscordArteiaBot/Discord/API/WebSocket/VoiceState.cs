﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public class VoiceState
	{
		public long		channel_id;
		public bool		deaf;
		public long		guild_id;
		public bool		mute;
		public bool		self_deaf;
		public bool		self_mute;
		//long session_id; // ??
		public bool		suppress;
		//string token; // ??
		public long		user_id;

		public VoiceState ()
		{
			
		}
	}
}

