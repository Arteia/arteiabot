﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Discord.API.WebSocket
{
	public struct MessageCreate
	{
		public string timestamp;
		public long nonce;
		public List<Mention> mentions;
		public bool mention_everyone;
		public long id;
		public List<Embed> embeds;
		public string edited_timestamp;
		public string content;
		public long channel_id;
		public User author;
		public List<Attachment> attachments;
	}
}

