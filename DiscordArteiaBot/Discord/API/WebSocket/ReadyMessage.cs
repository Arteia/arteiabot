﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public struct ReadyMessage
	{
		public string t;
		public int s;
		public int op;
		public Ready d;
	}
}

