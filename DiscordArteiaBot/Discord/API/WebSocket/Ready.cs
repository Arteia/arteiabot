﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Discord.API.WebSocket
{
	public class Ready
	{
		public int						v;
		public UserSettings				user_settings;
		public List<UserGuildSettings>	user_guild_settings;
		public User						user;
		public object					tutorial;
		public string					session_id;
		public List<ReadState>			read_state;
		public List<PrivateChannel>		private_channels;
		public int						heartbeat_interval;
		public List<Guild>				guilds;
	}
}

