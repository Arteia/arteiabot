﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public class Presence
	{
		public int		game_id;
		public string	status;
		public User		user;

		public Presence()
		{
		}
	}
}

