﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public struct ReadState
	{
		public int		mention_count;
		public string	id;
		public string	last_message_id;
	}
}

