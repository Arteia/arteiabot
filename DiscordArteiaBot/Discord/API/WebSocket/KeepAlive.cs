﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public class KeepAlive
	{
		public int op;
		public long d;

		public KeepAlive()
		{
			// Keep alive opcode
			this.op = 1;

			// Get current UTC date and time
			DateTime n = DateTime.UtcNow;

			// As milliseconds
			this.d = Convert.ToInt64(n.ToUniversalTime().Subtract(
				new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
			).TotalMilliseconds);
		}
	}
}

