﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public struct User
	{
		public bool		verified;
		public string	username;
		public long		id;
		public string	email;
		public int		discriminator;
		public string	avatar;
	}
}

