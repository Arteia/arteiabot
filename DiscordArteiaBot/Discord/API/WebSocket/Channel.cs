﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Discord.API.WebSocket
{
	public class Channel
	{
		public long							id;
		public string						last_message_id;
		public string						name;
		public List<PermissionOverwrite>	permission_overwrites;
		public int							position;
		public string						topic;
		public string						type;

		private static Net.DiscordWebConnection Connection;
		private static string URL = "https://discordapp.com/api/channels/{0}";

		public Channel () { }

		public Channel(long id)
		{
			this.id = id;
		}

		public static void bindConnection(Net.DiscordWebConnection connection)
		{
			Channel.Connection = connection;
		}

		public void sendMessage(string message)
		{
			API.Web.Message msg = new API.Web.Message (message);
			string url = String.Format (Channel.URL, this.id);
			url = url + msg.getURI ();
			Console.WriteLine ("Requesting : " + url);
			Console.WriteLine ("With       : " + msg.AsString ());
			Channel.Connection.request (url, msg.getRequestType (), msg.AsString());
		}
	}
}

