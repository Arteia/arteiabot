﻿using System;

namespace ArteiaBot.Discord.API.WebSocket
{
	public class UserSettings
	{
		public string	theme;
		public bool		show_current_game;
		public bool		render_embeds;
		public bool		message_display_compact;
		public string	locale;
		public bool		inline_embed_media;
		public bool		inline_attachment_media;
		public bool		enable_tts_command;
		public bool		convert_emoticons;

		public UserSettings ()
		{
			
		}
	}
}

