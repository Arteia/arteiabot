﻿using System;
using System.Collections.Generic;

namespace ArteiaBot.Discord.API.WebSocket
{
	public struct Connect
	{
		public string						token;
		public int							v;
		public Dictionary<string, string>	properties;
		public int							large_threshold;
		public bool							compress;

		public Connect(string token)
		{
			this.token = token;
			this.v = 3;
			this.properties = new Dictionary<string, string>();

			this.properties.Add ("$os", "Windows");
			this.properties.Add ("$browser", "Chrome");
			this.properties.Add ("$device", "");
			this.properties.Add ("$referrer", "https://discordapp.com/@me");
			this.properties.Add ("$referring_domain", "discordapp.com");

			this.large_threshold = 100;
			this.compress = false;
		}
	}
}

