﻿using System;

using Newtonsoft.Json;

using ArteiaBot.Bot.Event;
using System.Timers;

namespace ArteiaBot.Discord.Message
{
	public static class MessageHandler
	{
		private static EventLoop evtLoop;

		public static void bindEventLoop(ref EventLoop evLoop)
		{
			MessageHandler.evtLoop = evLoop;
		}

		public static void handle(string injest)
		{
			Discord.API.WebSocket.Message msg = JsonConvert.DeserializeObject<Discord.API.WebSocket.Message> (injest);
			switch (msg.t) {
			case "MESSAGE_CREATE":
				MessageHandler.emitMessageCreate (injest);
			break;
			case "READY":
				MessageHandler.emitReady (injest);
				break;
			default:
			break;
			}
		}

		private static void emitReady(string msg)
		{
			DiscordMessage dm = new DiscordMessage ();
			dm.setMessage (msg);
			MessageHandler.evtLoop.queueMessage (EventType.DISCORD_READY, dm);
		}

		private static void emitMessageCreate(string msg)
		{
			DiscordMessage dm = new DiscordMessage ();
			dm.setMessage (msg);
			MessageHandler.evtLoop.queueMessage (EventType.DISCORD_MESSAGE_CREATE, dm);
		}
	}
}

