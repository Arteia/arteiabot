﻿using System;

using Newtonsoft.Json;

using ArteiaBot.Bot.Message;

namespace ArteiaBot.Discord.Message
{
	public class DiscordMessage : AbstractMessage
	{
		public DiscordMessage ()
		{
			
		}

		public T getMessage<T>()
		{
			return JsonConvert.DeserializeObject<T> (this.message);
		}
	}
}

